﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Spomenici.models;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for RemoveType.xaml
    /// </summary>
    public partial class RemoveType : Window
    {
        public delegate Point GetPosition(IInputElement element);
        public ObservableCollection<MonType> MonTypes { get; set; }
        public RemoveType(ObservableCollection<MonType> types)
        {
            InitializeComponent();
            this.DataContext = this;
            MonTypes = types;
        }

        private void DeleteTypes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int index = GetCurrentRowIndex(e.GetPosition);
            MonType selectedType = MonTypes[index];
           // MessageBox.Show(selectedType.Desc);
          // open form
            EdremType erType = new EdremType(selectedType);
            erType.ShowDialog();
            // delete if need it
            if (erType.IsDelete)
                MonTypes.RemoveAt(index);
        }

        /// <summary>
        /// returns index of clicked row 
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        private int GetCurrentRowIndex(GetPosition pos)
        {
            int curIndex = -1;
            for (int i = 0; i < DeleteTypes.Items.Count; i++)
            {
                DataGridRow itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }

       /// <summary>
       /// 
       /// </summary>
       /// <param name="theTarget"></param>
       /// <param name="position"></param>
       /// <returns></returns>
        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            Point point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private DataGridRow GetRowItem(int index)
        {
            if (DeleteTypes.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return DeleteTypes.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }
    }
}
