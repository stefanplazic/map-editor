﻿using Microsoft.Win32;
using Spomenici.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for EdremType.xaml
    /// </summary>
    public partial class EdremType : Window
    {
        public MonType SomeType { get; set; }
        public bool IsDone { get; set; }
        public bool IsDelete { get; set; }

        public EdremType(MonType someType)
        {
            InitializeComponent();
            SomeType = someType;
            //set booleans
            IsDone = false;
            IsDelete = false;
            //set bindgs
            this.DataContext = SomeType;
        }

        /// <summary>
        /// enables user to choose new type icon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void iconchoseBtn_Click(object sender, RoutedEventArgs e)
        {
            //open file chooser
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = " *.jpg | *.jpg | *.jpeg | *.jpeg | *.png | *.png";
            if (dialog.ShowDialog() == true)
            {
                //set new icon 
                SomeType.Icon = dialog.FileName;
                
            }
        }

        /// <summary>
        /// user is done eiditing , and wants to close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doneBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// enables user to delete data, with message prompt question first
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            string messageTitle = "Do you want to delete ?";

            MessageBoxButton canyesbtn = MessageBoxButton.YesNo;
            MessageBoxImage messimg = MessageBoxImage.Question;

            MessageBoxResult res = MessageBox.Show(messageTitle, "Exit dialog", canyesbtn, messimg);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    IsDelete = true;
                    this.Close();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }
    }
}
