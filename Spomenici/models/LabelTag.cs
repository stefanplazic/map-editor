﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;


namespace Spomenici.models
{
    [Serializable]
    public class LabelTag
    {
        private String id;
        private String colour;
        private String desc;
        private Guid guiID;

       
        public LabelTag() { }

        public Guid GuiID
        {
            get { return guiID; }
            set { guiID = value; }
        }
        public String Desc
        {
            get { return desc; }
            set { desc = value; }
        }

        public String Colour
        {
            get { return colour; }
            set { colour = value; }
        }

        public String Id
        {
            get { return id; }
            set { id = value; }
        }
        

        

    }
}
