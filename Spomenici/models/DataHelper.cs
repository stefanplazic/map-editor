﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Spomenici.models
{
  public  class DataHelper
    {
        private readonly string _datoteka = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "repozitorijum.spomenici");
        private readonly string _datoteka1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "repozitorijum.labele");
        private readonly string _datoteka2 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "repozitorijum.monotypes");

        public static ObservableCollection<Monument> Monuments
        {
            get;
            set;
        }
        public static ObservableCollection<LabelTag> MonoLabels
        {
            get;
            set;
        }
        public static ObservableCollection<MonType> MonoTypes
        {
            get;
            set;
        }

      //getters and setters


        public DataHelper() {

            if (File.Exists("monotypes.xml"))
            {
                XmlSerializer serializertip = new XmlSerializer(typeof(ObservableCollection<MonType>));
                using (TextReader textReader = new StreamReader("monotypes.xml"))
                {

                    MonoTypes = serializertip.Deserialize(textReader) as ObservableCollection<MonType>;
                }
            }
            else
                MonoTypes = new ObservableCollection<MonType>();
            /*LABELS*/
            if (File.Exists("labele.xml"))
            {
                XmlSerializer serializertip = new XmlSerializer(typeof(ObservableCollection<LabelTag>));
                using (TextReader textReader = new StreamReader("labele.xml"))
                {

                    MonoLabels = serializertip.Deserialize(textReader) as ObservableCollection<LabelTag>;
                }
            }
            else
                MonoLabels = new ObservableCollection<LabelTag>();


            if (File.Exists("spomenici.xml"))
            {
                XmlSerializer serializertip = new XmlSerializer(typeof(ObservableCollection<Monument>));
                using (TextReader textReader = new StreamReader("spomenici.xml"))
                {

                    Monuments = serializertip.Deserialize(textReader) as ObservableCollection<Monument>;
                }
            }
            else
                Monuments = new ObservableCollection<Monument>();



        }

        public void Save()
        {

            // MemorisiDatoteku(_datoteka,Resursi);
            // MemorisiDatoteku(_datoteka1, Etikete);
            // MemorisiDatoteku(_datoteka2, Tipovi);

            XmlSerializer serializert = new XmlSerializer(DataHelper.MonoTypes.GetType());
            using (TextWriter textWriter = new StreamWriter("monotypes.xml"))
            {

                serializert.Serialize(textWriter, DataHelper.MonoTypes);
            }

            XmlSerializer serializer = new XmlSerializer(DataHelper.MonoLabels.GetType());
            using (TextWriter textWriter = new StreamWriter("labele.xml"))
            {

                serializer.Serialize(textWriter, DataHelper.MonoLabels);
            }

            XmlSerializer serializee = new XmlSerializer(DataHelper.Monuments.GetType());
            using (TextWriter textWriter = new StreamWriter("spomenici.xml"))
            {

                serializee.Serialize(textWriter, DataHelper.Monuments);
            }



        }

        public void LoadAll() {
            LoadLabels();
            LoadMonoTypes();
            LoadMonuments();

            return;
        }

     
      private void LoadMonuments()
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = null;

            if (File.Exists(_datoteka))
            {
                try
                {
                    stream = File.Open(_datoteka, FileMode.Open);

                    Monuments = (ObservableCollection<Monument>)formatter.Deserialize(stream);

                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception in loading monuments list from file!" + e.Message);

                }
                finally
                {
                    if (stream != null)
                        stream.Dispose();
                }

            }
            else
                Monuments = new ObservableCollection<Monument>();
        }

      /*load labels*/

      private void LoadLabels()
      {
          BinaryFormatter formatter = new BinaryFormatter();
          FileStream stream = null;

          if (File.Exists(_datoteka1))
          {
              try
              {
                  stream = File.Open(_datoteka, FileMode.Open);

                  MonoLabels = (ObservableCollection<LabelTag>)formatter.Deserialize(stream);

              }
              catch (Exception e)
              {
                  Console.WriteLine("Exception in loading labels list from file!" + e.Message);

              }
              finally
              {
                  if (stream != null)
                      stream.Dispose();
              }

          }
          else
              MonoLabels = new ObservableCollection<LabelTag>();
      }

      /*LOAD MONOTYPES*/
      private void LoadMonoTypes()
      {
          BinaryFormatter formatter = new BinaryFormatter();
          FileStream stream = null;

          if (File.Exists(_datoteka2))
          {
              try
              {
                  stream = File.Open(_datoteka, FileMode.Open);

                  MonoTypes = (ObservableCollection<MonType>)formatter.Deserialize(stream);

              }
              catch (Exception e)
              {
                  Console.WriteLine("Exception in loading MONO TYPES list from file!" + e.Message);

              }
              finally
              {
                  if (stream != null)
                      stream.Dispose();
              }

          }
          else
              MonoTypes = new ObservableCollection<MonType>();
      }
    }
}
