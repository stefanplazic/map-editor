﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;


namespace Spomenici.models
{
    [Serializable]
    public class MonType : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        private String id;
        private String name;
        private String desc;
        private String icon;
        private Guid guiID;

        public MonType() { }

        public Guid GuiID
        {
            get { return guiID; }
            set { guiID = value; OnPropertyChanged("GuID"); }
        }
        public String Icon
        {
            get { return icon; }
            set { icon = value; OnPropertyChanged("Icon"); }
        }
        public String Desc
        {
            get { return desc; }
            set { desc = value; OnPropertyChanged("Desc"); }
        }
        
        public String Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        public String Id
        {
            get { return id; }
            set { id = value; OnPropertyChanged("Id"); }
        }
       
       
       
    }
}
