﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Spomenici.models
{
    [Serializable]
   public enum Climate 
   { polar, [Description("Polar")]
   continental,[Description("Continental")]
   temperate_continental,[Description("Temperate Continental")]
   desert, [Description("Desert")]
   subtropical,[Description("subtropical")]
   tropical 
    
   
}
    [Serializable]
   public enum Turistic_status { exploited, available, unavailable }

    [Serializable]
    [System.Xml.Serialization.XmlInclude(typeof(MonType))]
    [System.Xml.Serialization.XmlInclude(typeof(LabelTag))]
   public class Monument : INotifyPropertyChanged 
   {
       private String id;
       private String name;
       private String desc;
       private String icon;
       private bool ecolog_threatend;
       private bool threatend_spices;
       private bool populated_region;
       private Climate clime;
       private DateTime date;
       private Turistic_status status;
       private decimal income;
       private List<LabelTag> labels  = new List<LabelTag>();
       private MonType type;
       private Location loc;
       private LabelTag labeltag;

       private Guid guiID;

      
      

       public Monument() { }

       public Guid GuiID
       {
           get { return guiID; }
           set { guiID = value; }
       }
       public LabelTag Labeltag
       {
           get { return labeltag; }
           set { labeltag = value; }
       }

       public Location Loc
       {
           get { return loc; }
           set { loc = value; RaiseProperChanged(); }
       }
       public String Id
       {
           get { return id; }
           set { id = value; RaiseProperChanged(); }
       }
       

       public String Name
       {
           get { return name; }
           set { name = value; RaiseProperChanged(); }
       }
      

       public String Desc
       {
           get { return desc; }
           set { desc = value; RaiseProperChanged(); }
       }
       

       public String Icon
       {
           get { return icon; }
           set { icon = value; RaiseProperChanged(); }
       }
       

       public bool Ecolog_threatend
       {
           get { return ecolog_threatend; }
           set { ecolog_threatend = value; RaiseProperChanged(); }
       }
       

       public bool Threatend_spices
       {
           get { return threatend_spices; }
           set { threatend_spices = value; RaiseProperChanged(); }
       }
       

       public bool Populated_region
       {
           get { return populated_region; }
           set { populated_region = value; RaiseProperChanged(); }
       }
      

       public Climate Clime
       {
           get { return clime; }
           set { clime = value; RaiseProperChanged(); }
       }
       

       public DateTime Date
       {
           get { return date; }
           set { date = value; RaiseProperChanged(); }
       }
       

       public Turistic_status Status
       {
           get { return status; }
           set { status = value; RaiseProperChanged(); }
       }
       

       public decimal Income
       {
           get { return income; }
           set { income = value; RaiseProperChanged(); }
       }


       public List<LabelTag> Labels
       {
           get { return labels; }
           set { labels = value; RaiseProperChanged(); }
       }
       

       public MonType Type
       {
           get { return type; }
           set { type = value; RaiseProperChanged(); }
       }

       public event PropertyChangedEventHandler PropertyChanged;
       /*call when property is changed*/
       private void RaiseProperChanged([CallerMemberName] string caller = "")
       {
           if (PropertyChanged != null)
           {
               PropertyChanged(this, new PropertyChangedEventArgs(caller));
           }
       }
   }
}
