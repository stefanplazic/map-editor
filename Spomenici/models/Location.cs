﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spomenici.models
{
    [Serializable]
   public class Location
    {
       public Location() { }
       public Location(double x, double y) 
       {
           Y = y;
           X = x;
       }

       private double x;
       private double y;

       public double Y
       {
           get { return y; }
           set { y = value; }
       }

       public double X
       {
           get { return x; }
           set { x = value; }
       }

    }
}
