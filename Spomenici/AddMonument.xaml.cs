﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Spomenici.models;
using System.Collections.ObjectModel;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for AddMonument.xaml
    /// </summary>
    public partial class AddMonument : Window
    {
        public String MonumentIcon { get; set; }
        public DateTime DiscoveryDate { get; set; }
        public Monument Mon { get; set; }
        public bool WorkWith { get; set; }
        public ObservableCollection<MonType> MonTypes { get; set; }
        private bool IsPicture = false; // flag for if picture set
        public AddMonument(ObservableCollection<MonType> types)
        {
            MonTypes = types;
            InitializeComponent();
            WorkWith = false;
           
            setTypesBox();
            
            
        }

        //set type box
        private void setTypesBox() {
            foreach (MonType item in MonTypes)
            {
                typeBox.Items.Add(item.Name);
            }
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            //ako korisnik zeli da izadje
            string messageTitle = "Do you want to close the form?";

            MessageBoxButton canyesbtn = MessageBoxButton.YesNo;
            MessageBoxImage messimg = MessageBoxImage.Question;

            MessageBoxResult res = MessageBox.Show(messageTitle, "Exit dialog", canyesbtn, messimg);
            switch(res)
            {
                case MessageBoxResult.Yes:
                    this.Close();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (regularCheck())
            {
                WorkWith = true;
                this.Close();
            }
              
        }


        private bool regularCheck() {

            bool good = true;
            String id = IDBox.Text;
            String name = nameBox.Text;
            String description = descBox.Text;
            String climeType = climateBox.SelectedValue.ToString();
            bool ecologicalyCheck = ecologicalyChk.IsChecked.Value;
            bool animals_check = animalsChk.IsChecked.Value;
            bool regions_Chk = regionsChk.IsChecked.Value;
            decimal income = Convert.ToDecimal(incomeBox.Text);
            String typeNa = typeBox.SelectedValue.ToString();
            String turistic_status = statusBox.SelectedValue.ToString();

            //adding monument
            Monument mo = new Monument();
            mo.Date = DiscoveryDate;
            mo.Desc = description;
            mo.Ecolog_threatend = ecologicalyCheck;
            mo.Threatend_spices = animals_check;
            mo.Populated_region = regions_Chk;
            mo.Income = income;
            mo.Name = name;
            mo.Icon = MonumentIcon;
            //set the climate type
            switch (climeType)
            {
                case "Polar":
                    mo.Clime = Climate.polar; 
                    break;
                case "Continental":
                    mo.Clime = Climate.continental;
                    break;
                case "Temperate":
                    mo.Clime = Climate.temperate_continental;
                    break;
                case "Desert":
                    mo.Clime = Climate.desert;
                    break;
                case "Subtropical":
                    mo.Clime = Climate.subtropical;
                    break;
                case "Tropical":
                    mo.Clime = Climate.tropical;
                    break;
            }

            //serach types
            var typeObj = MonTypes.Where(X => X.Name == typeNa).First();
            if (typeObj == null)
                good = false;
            else
            { 
                //cast object as type
                mo.Type = (MonType)typeObj;
                if (!IsPicture)
                    mo.Icon = mo.Type.Icon;
                    
            }
            //make turistic status

            switch (turistic_status)
            {
                case "Explotied" :
                    mo.Status = Turistic_status.exploited;
                    break;

                case "Available":
                    mo.Status = Turistic_status.available;
                    break;
                case "Unavailable":
                    mo.Status = Turistic_status.unavailable;
                    break;
                default:
                    break;
            }

            //set the object as property
            Mon = mo;
            return good;
        }

        private void iconBtn_Click(object sender, RoutedEventArgs e)
        {
            //open file chooser
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = " *.jpg | *.jpg | *.jpeg | *.jpeg | *.png | *.png";
            if (dialog.ShowDialog() == true)
                MonumentIcon = dialog.FileName;
            IsPicture = true;
            
        }
    }
}
