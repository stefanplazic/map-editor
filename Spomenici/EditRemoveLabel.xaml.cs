﻿using Spomenici.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for EditRemoveLabel.xaml
    /// </summary>
    public partial class EditRemoveLabel : Window
    {
        public ObservableCollection<LabelTag> Labelss { get; set; }
        

        public EditRemoveLabel(ObservableCollection<LabelTag> lab)
        {
            Labelss = lab;
            InitializeComponent();
            this.DataContext = Labelss;
        }
    }
}
