﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for AddMonumentType.xaml
    /// </summary>
    public partial class AddMonumentType : Window
    {

        public String IconType { get; set; }
        public String TypeName { get; set; }
        public String Description { get; set; }
        public String ID { get; set; }
        public bool ReadyTo { get; set; }
        public AddMonumentType()
        {
            InitializeComponent();
            IconType = "";
            ReadyTo = false;
            
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            //ako korisnik zeli da izadje
            string messageTitle = "Do you want to close the form?";

            MessageBoxButton canyesbtn = MessageBoxButton.YesNo;
            MessageBoxImage messimg = MessageBoxImage.Question;

            MessageBoxResult res = MessageBox.Show(messageTitle, "Exit dialog", canyesbtn, messimg);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    this.Close();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void DoneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (check())
            {
                ReadyTo = true;
                this.Close();
            }
            else {
                MessageBox.Show("Must eneter data in all fields");
            }
        }

        private bool check() {
            ID = IDBox.Text;
            TypeName = NameBox.Text;
            Description = DescriptionBox.Text;
            if (IconType.Length < 1 || ID.Length < 1 || TypeName.Length < 2 || Description.Length < 5)
                return false;

            return true;
        }

        private void iconchoseBtn_Click(object sender, RoutedEventArgs e)
        {
            //open file chooser
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = " *.jpg | *.jpg | *.jpeg | *.jpeg | *.png | *.png";
            if (dialog.ShowDialog() == true)
            {
                IconType = dialog.FileName;
                iconnameBox.Text = System.IO.Path.GetFileName(IconType);
            }
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            string filename = (string)((DataObject)e.Data).GetFileDropList()[0];
           // MessageBox.Show(System.IO.Path.GetExtension(filename));

            //check if file is image
            List<String> imageExtensions = new List<string> { ".PNG",".JPG","JPEG"};
            if (imageExtensions.Contains(System.IO.Path.GetExtension(filename).ToUpperInvariant())) {
                IconType = filename;
            }
            else
                MessageBox.Show("Not image :(");
            
            
        }

        private void Grid_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.Bitmap) )
            {
                e.Effects = DragDropEffects.None;
            }
            else
                e.Effects = DragDropEffects.All;
        }

        private void Grid_DragOver(object sender, DragEventArgs e)
        {
           
        }
    }
}
