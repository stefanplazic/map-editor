﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for AddLabel.xaml
    /// </summary>
    public partial class AddLabel : Window
    {
        public bool IsDone { get; set; }
        private VisualBrush brush;
        public string BurshName { get; set; }

        public VisualBrush Brush
        {
            get { return brush; }
            set { brush = value; }
        }
        private string desc;

        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }
        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        public AddLabel()
        {
            InitializeComponent();
            IsDone = false;
            //add even to combobox
            
        }
        /// <summary>
        /// Close the form , with popup dialog question
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            //ako korisnik zeli da izadje
            string messageTitle = "Do you want to close the form?";

            MessageBoxButton canyesbtn = MessageBoxButton.YesNo;
            MessageBoxImage messimg = MessageBoxImage.Question;

            MessageBoxResult res = MessageBox.Show(messageTitle, "EXIT DIALOG", canyesbtn, messimg);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    this.Close();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void DoneBtn_Click(object sender, RoutedEventArgs e)
        {
            if (check())
            {
                IsDone = true;
                this.Close();

            }
            else 
            {
                MessageBoxButton canyesbtn = MessageBoxButton.OK;
                MessageBoxImage messimg = MessageBoxImage.Error;

                MessageBoxResult res = MessageBox.Show("Most fill all fields!", "ERROR DIALOG", canyesbtn, messimg);
            }
        }
        /// <summary>
        /// returns true if all elements are filled with valide data, otherwise - false
        /// </summary>
        /// <returns></returns>
        private bool check() 
        {
            id = IDBox.Text;
            desc = DescBox.Text;
            if (id != null && id != "" && desc != null && desc != "" && brush != null)
                return true;
            return false;
        }

        private void ChooseBtn_Click(object sender, RoutedEventArgs e)
        {
            MakeBrush br = new MakeBrush();
            br.ShowDialog();
            if (br.IsDone)
            {
                BurshName = br.BrushName;
                brush = br.MyBrush;
                colorBlock.Background = brush;
            }
        }

       

        
    }
}
