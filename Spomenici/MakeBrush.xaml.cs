﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for MakeBrush.xaml
    /// </summary>
    public partial class MakeBrush : Window
    {
        public VisualBrush MyBrush { get; set; }//return the value of selected brush
        public Boolean IsDone { get; set; }
        public string BrushName { get; set; }
        public MakeBrush()
        {
            InitializeComponent();
            IsDone = false; //init as false
            
        }

        private void colorBlack_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock blok = (TextBlock)sender; //cast sender to textblock component
            string nameBlok = blok.Name; // get the name from textblok
            

            //set transparent background
            Brush b = blok.Background;
            b.Opacity = 0.5;          

            switch (nameBlok)
            {
                case "colorBlack":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualBlack");
                    IsDone = true;
                    BrushName = "MyVisualBlack";
                    break;
                case "colorGreen":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualGreen");
                    IsDone = true;
                    BrushName = "MyVisualGreen";
                    break;
                case "colorYellow":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualYellow");
                    IsDone = true;
                    BrushName = "MyVisualYellow";
                    break;
                case "colorOrange":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualOrange");
                    IsDone = true;
                    BrushName = "MyVisualOrange";
                    break;
                case "MyVisualBlue":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualBlue");
                    IsDone = true;
                    BrushName = "MyVisualBlue";
                    break;
                case "colorPurple":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualPurple");
                    IsDone = true;
                    BrushName = "MyVisualPurple";
                    break;
                case "colorAzura":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualAzure");
                    IsDone = true;
                    BrushName = "MyVisualAzure";
                    break;
                case "colorBrown":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualBrown");
                    IsDone = true;
                    BrushName = "MyVisualBrown";
                    break;
                case "colorGolden":
                    MyBrush = (VisualBrush)this.FindResource("MyVisualGolden");
                    IsDone = true;
                    BrushName = "MyVisualGolden";
                    break;
                default:
                    break;
            }

        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

      
        


    }
}
