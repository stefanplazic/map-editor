﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Spomenici.validation
{
   public class ValidationRulle: ValidationRule
    {
       public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
       {
           try
           {
               String val = value as String;
               if (val.Length >2)
               {
                   return new ValidationResult(true,null);
               }
               return new ValidationResult(false, "Must have at least 3 characters");
           }
           catch (Exception)
           {

               return new ValidationResult(false, "Unknown error occured");
           }

          
       }     

    }
}
