﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Spomenici.models;
using System.Collections.ObjectModel;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Controls.Primitives;
using System.Xml.Serialization;
using Spomenici.help;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        

        private String filePath ="";
        int rowIndex = -1;
        public delegate Point GetPosition(IInputElement element);
        public MainWindow()
        {
            InitializeComponent();

            //opening while loading 
            openMonoType();
            openLabelTag();
            openMonument();
            
            tabela.DataContext = Monuments;

            
        }
       

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void menuOpen_Click(object sender, RoutedEventArgs e)
        {
            //load the files
            openMonoType();
            openLabelTag();
            openMonument();
            
        }
        public void openMonoType() {
            BinaryFormatter formatter = new BinaryFormatter();

            filePath = "monotypes.xml";
            if (File.Exists(filePath))
            {
                XmlSerializer serializee = new XmlSerializer(MonoTypes.GetType());
                using (TextReader textReader = new StreamReader("monotypes.xml"))
                {

                    monoTypes = (ObservableCollection<MonType>)serializee.Deserialize(textReader);
                }

               

            }
            else
                monoTypes = new ObservableCollection<MonType>();
        }


        /// <summary>
        /// method for loading LabelTag class from xml file
        /// </summary>
        public void openLabelTag()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            filePath = "labels.xml";
            if (File.Exists(filePath))
            {
                XmlSerializer serializee = new XmlSerializer(LabelTags.GetType());
                using (TextReader textReader = new StreamReader("labels.xml"))
                {

                    LabelTags = (ObservableCollection<LabelTag>)serializee.Deserialize(textReader);
                }



            }
            else
                LabelTags = new ObservableCollection<LabelTag>();
        }


        public void openMonument()
        {
            BinaryFormatter formatter = new BinaryFormatter();

            filePath = "mono.xml";
            if (File.Exists(filePath))
            {
                try
                {
                    XmlSerializer serializee = new XmlSerializer(Monuments.GetType());
                    using (TextReader textReader = new StreamReader(filePath))
                    {

                        Monuments = (ObservableCollection<Monument>)serializee.Deserialize(textReader);
                        //if evriting is allright - show icons on map
                        Update_Canvas(Monuments);
                    }
                }
                catch (Exception ex)
                {
                    DumpException(ex);
                }



            }
            else
                Monuments = new ObservableCollection<Monument>();
        }
       

        private void menuSave_Click(object sender, RoutedEventArgs e)
        {
            //save lists to file

            SaveMonoTypes();
            SaveLabelTags();
            SaveMonuments();
           // SaveMonuments();
        }

        /// <summary>
        /// Seraialize monotypes collection to xml file
        /// </summary>
        public void SaveMonoTypes()
        {
            XmlSerializer serializee = new XmlSerializer(MonoTypes.GetType());
            using (TextWriter textWriter = new StreamWriter("monotypes.xml"))
            {

                serializee.Serialize(textWriter, MonoTypes);
            }
        }

        /// <summary>
        /// Seraialize LabelTag collection to xml file
        /// </summary>
        public void SaveLabelTags()
        {
            XmlSerializer serializee = new XmlSerializer(LabelTags.GetType());
            using (TextWriter textWriter = new StreamWriter("labels.xml"))
            {

                serializee.Serialize(textWriter, LabelTags);
            }
        }

        /// <summary>
        /// Seraialize Monuments collection to xml file
        /// </summary>
        public void SaveMonuments()
        {
            try
            {
                XmlSerializer serializee = new XmlSerializer(Monuments.GetType());
                using (TextWriter textWriter = new StreamWriter("mono.xml"))
                {

                    serializee.Serialize(textWriter, Monuments);
                }
            }
            catch (Exception ex)
            {

                DumpException(ex);
            }
            
        }

        /*EXCEPTIONS*/


        public static void DumpException(Exception ex)
        {
            Console.WriteLine("--------- Outer Exception Data ---------");
            WriteExceptionInfo(ex);
            ex = ex.InnerException;
            if (null != ex)
            {
                Console.WriteLine("--------- Inner Exception Data ---------");
                WriteExceptionInfo(ex.InnerException);
                ex = ex.InnerException;
            }
        }
        public static void WriteExceptionInfo(Exception ex)
        {
            Console.WriteLine("Message: {0}", ex.Message);
            MessageBox.Show(ex.Message+"---------------- type"+ ex.GetType().FullName);
            Console.WriteLine("Exception Type: {0}", ex.GetType().FullName);
            Console.WriteLine("Source: {0}", ex.Source);
            Console.WriteLine("StrackTrace: {0}", ex.StackTrace);
            Console.WriteLine("TargetSite: {0}", ex.TargetSite);
        }


        private void menuExit_Click(object sender, RoutedEventArgs e)
        {
            //save all data before exiting
            SaveLabelTags();
            SaveMonoTypes();
            SaveMonuments();
            this.Close();
        }

        private void addMonument_Click(object sender, RoutedEventArgs e)
        {
            AddMonument win = new AddMonument(MonoTypes);
            //set observable collection of types
            
            win.ShowDialog();
            if (win.WorkWith == true) 
            {
                //did user added monument
                Monument monument_to_ad = win.Mon;
                //add guiId if it doesn't exists
                if (monument_to_ad.GuiID == Guid.Empty)
                    monument_to_ad.GuiID = Guid.NewGuid();
                //append to list
                Monuments.Add(monument_to_ad);
            } 
        }

        

        private void addTag_Click(object sender, RoutedEventArgs e)
        {
            AddLabel adLabel = new AddLabel();
            adLabel.ShowDialog();
            if (adLabel.IsDone)
            {
                LabelTag lt = new LabelTag();
                lt.Colour = adLabel.BurshName;
                
                lt.Id = adLabel.Id;
                if (lt.GuiID == Guid.Empty)
                    lt.GuiID = Guid.NewGuid();
                labelTags.Add(lt);
                
            }
        }

        private void addType_Click(object sender, RoutedEventArgs e)
        {
            AddMonumentType monType = new AddMonumentType();
           
           
            
            monType.ShowDialog();
            if (monType.ReadyTo)
            {
                MonType mtype = new MonType();
                mtype.Id = monType.ID;
                mtype.Icon = monType.IconType;
                mtype.Name = monType.TypeName;
                mtype.Desc = monType.Description;

                //add guiId if it doesn't exists
                if (mtype.GuiID == Guid.Empty)
                    mtype.GuiID = Guid.NewGuid();

                MonoTypes.Add(mtype);
               
            }
        }

        //list of objects
        private ObservableCollection<Monument> monuments = new ObservableCollection<Monument>();

        public ObservableCollection<Monument> Monuments
        {
            get { return monuments; }
            set { monuments = value; }
        }

        public ObservableCollection<Monument> ReservMonument { get; set; }

        private ObservableCollection<MonType> monoTypes = new ObservableCollection<MonType>();

        public ObservableCollection<MonType> MonoTypes
        {
            get { return monoTypes; }
            set { monoTypes = value; }
        }

        private ObservableCollection<LabelTag> labelTags = new ObservableCollection<LabelTag>();

        public ObservableCollection<LabelTag> LabelTags
        {
            get { return labelTags; }
            set { labelTags = value; }
        }

        

        private void removeTag_Click(object sender, RoutedEventArgs e)
        {
            //edit or remove labels| tags
            EditRemoveLabel erLabel = new EditRemoveLabel(LabelTags);
            
            erLabel.ShowDialog();
        }

        private void removeType_Click(object sender, RoutedEventArgs e)
        {
            
            //call RemoveType form
            RemoveType remove_type = new RemoveType(MonoTypes);
            remove_type.ShowDialog();
        }

        private void tabela_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            rowIndex = GetCurrentRowIndex(e.GetPosition);
            if (rowIndex < 0)
                return;
            tabela.SelectedIndex = rowIndex;
            Monument selectedEmp = tabela.Items[rowIndex] as Monument;
            if (selectedEmp == null || selectedEmp.Loc!= null)
                return;
            BitmapImage i;

            if (selectedEmp.Icon != null)
            {
                i = new BitmapImage(new Uri(selectedEmp.Icon));
            }
            
            else
                return;

            DataObject dragData = new DataObject("myFormat", i);
            DragDrop.DoDragDrop(tabela, dragData, DragDropEffects.Copy);
        }
        private bool GetMouseTargetRow(Visual theTarget, GetPosition position)
        {
            Rect rect = VisualTreeHelper.GetDescendantBounds(theTarget);
            Point point = position((IInputElement)theTarget);
            return rect.Contains(point);
        }
        private int GetCurrentRowIndex(GetPosition pos)
        {
            int curIndex = -1;
            for (int i = 0; i < tabela.Items.Count; i++)
            {
                DataGridRow itm = GetRowItem(i);
                if (GetMouseTargetRow(itm, pos))
                {
                    curIndex = i;
                    break;
                }
            }
            return curIndex;
        }
        private DataGridRow GetRowItem(int index)
        {
            if (tabela.ItemContainerGenerator.Status
                    != GeneratorStatus.ContainersGenerated)
                return null;
            return tabela.ItemContainerGenerator.ContainerFromIndex(index)
                                                            as DataGridRow;
        }

        private void Canvas_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent("myFormat") || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        /// on dropp draw image on canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Canvas_Drop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent("myFormat"))
            {
                BitmapImage image = e.Data.GetData("myFormat") as BitmapImage;
                Image imageControl = new Image() { Width = 70, Height = 70, Source = image, HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Top };

                imageControl.Margin = new Thickness(e.GetPosition(canv).X, e.GetPosition(canv).Y, 0, 0);
                canv.Children.Add(imageControl);

                //add positition to property
                Location lo = new Location(e.GetPosition(canv).X, e.GetPosition(canv).Y);
                Monuments[rowIndex].Loc = lo;

            }
        }

        /// <summary>
        /// erase all images from canvas
        /// </summary>
        private void Erase_Canvas() 
        {
            var images = canv.Children.OfType<Image>().ToList();
            foreach (var image in images)
            {
                canv.Children.Remove(image);
            }
        }

        /// <summary>
        /// populate canvas with new elems
        /// </summary>
        /// <param name="mm"></param>
        private void Update_Canvas(ObservableCollection<Monument> mm)
        {
            Erase_Canvas();
           
            foreach (Monument item in mm)
            {
                BitmapImage bImage =  new BitmapImage(new Uri(item.Icon)); // convert icon string to bitmap image

                Image imageControl = new Image() { Width = 70, Height = 70, Source = bImage, HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Top };
                imageControl.Margin = new Thickness(item.Loc.X, item.Loc.Y, 0, 0);
                canv.Children.Add(imageControl);
            }
            
        }

        /// <summary>
        /// search collection for elems witch conatins typed name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchBtn_Click(object sender, RoutedEventArgs e)
        {
            //get text from box
            String serchText = searchBox.Text;
            // if there is serch string
            if (serchText.Length != 0)
            {

                //search the list
                ReservMonument = new ObservableCollection<Monument>(Monuments.Where(X => X.Name.StartsWith(serchText)));

                //set new binding
                tabela.ItemsSource = ReservMonument;
                //upade gridviw
                tabela.Items.Refresh();
                Update_Canvas(ReservMonument);
            }
            else
            {
                tabela.ItemsSource = Monuments;
                //upade gridviw
                tabela.Items.Refresh();
                Update_Canvas(Monuments);
            }
           
            
            

        }

        private void tabela_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int index = GetCurrentRowIndex(e.GetPosition);
            Monument selectedMon = Monuments[index];
            //MessageBox.Show(selectedMon.Desc);
            EditDelMonument edMon = new EditDelMonument(selectedMon);
            edMon.ShowDialog();
            if (edMon.IsDelete)
                Monuments.RemoveAt(index);

            Update_Canvas(Monuments);
        }

        private void helpItem_Click(object sender, RoutedEventArgs e)
        {
            //show main Helper
            MainHelper helper = new MainHelper();
            helper.Show();
        }

    }
}
