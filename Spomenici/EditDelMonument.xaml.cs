﻿using Microsoft.Win32;
using Spomenici.models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Spomenici
{
    /// <summary>
    /// Interaction logic for EditDelMonument.xaml
    /// </summary>
    public partial class EditDelMonument : Window
    {
        public Monument Mono { get; set; }
        public bool IsDelete { get; set; }
        public EditDelMonument(Monument mono)
        {
            
            InitializeComponent();
            Mono = mono;
            this.DataContext = Mono;
            IsDelete = false;
        }

        private void DeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            string messageTitle = "Do you want to delete ?";

            MessageBoxButton canyesbtn = MessageBoxButton.YesNo;
            MessageBoxImage messimg = MessageBoxImage.Question;

            MessageBoxResult res = MessageBox.Show(messageTitle, "Exit dialog", canyesbtn, messimg);
            switch (res)
            {
                case MessageBoxResult.Yes:
                    IsDelete = true;
                    this.Close();
                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void saveBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void iconBtn_Click(object sender, RoutedEventArgs e)
        {
            //open file chooser
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = " *.jpg | *.jpg | *.jpeg | *.jpeg | *.png | *.png";
            if (dialog.ShowDialog() == true)
            {
                //set new icon 
                Mono.Icon = dialog.FileName;

            }
        }
    }
}
