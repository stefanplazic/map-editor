Map Editor is the app developed in C# programming language using WPF. 

The user  can make monuments (giving it name, description,type, he can choose icon, and put monument to same category ), after it's creation user can drag and drop monument on map.
He can modify information about monument or to delete it.

User can also search monuments by entering it's name into search box, and choosing from witch category to search it.

During closing all data is save into xml file resource.

Project still can be upgraded: like adding capabilities for changing icon location already on map (this feature it still dosen't support ),
or like adding better user help system than current. 